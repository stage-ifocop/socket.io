const {createServer} = require('http');
const {Server} = require('socket.io');
const dotenv = require('dotenv');
const path = require('path');
dotenv.config(); // Charger les variables d'environnement
const express = require('express');
const app = express();

const httpServer = createServer(app);

// const io = new Server(httpServer, {
// 	cors: {
// 		origin:
// 			process.env.NODE_ENV !== 'development'
// 				? [
// 						'http://localhost:8080',
// 						'http://127.0.0.1:8080',
// 						'http://localhost:5501',
// 						'http://127.0.0.1:5501',
// 						'http://127.0.0.1:5555',
// 						'http://localhost:5555',
// 				  ]
// 				: 'https://www.eliazoura.fr',
// 	},
// });

const chatFilePath = path.join(__dirname, `/front/chat.html`);

// const publicPath = require('./app/chat.html');
console.log('🚀 ~ chatFilePath:', chatFilePath);

const publicRessources = path.join(__dirname, `/front/public`);
console.log('🚀 ~ publicRessources:', publicRessources);

// Définir le répertoire des fichiers statiques
app.use('', express.static(publicRessources));

app.get('/', (req, res) => {
	res.sendFile(chatFilePath);
});

const io = new Server(httpServer, {
	cors: {
		origin: '*',
	},
});

io.on('connection', (socket) => {
	const userID = socket.id.substring(0, 5);
	console.log('USER connecté : ' + userID);

	socket.on('message', (data) => {
		console.log(data);
		io.emit('message', {user: userID, text: data});
	});

	socket.on('disconnect', () => {
		console.log('USER déconnecté :' + userID);
	});

	// Vous pouvez ajouter plus de logique ici pour gérer les événements et les messages
});

// Démarrer le serveur HTTP
const PORT = process.env.PORT || 8080;
httpServer.listen(PORT, () => {
	console.log(`Server is running on port ${PORT}`);
});
