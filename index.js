const {createServer} = require('http');
const {Server} = require('socket.io');
const dotenv = require('dotenv');

dotenv.config(); // Charger les variables d'environnement

const httpServer = createServer();

const io = new Server(httpServer, {
	cors: {
		origin:
			process.env.NODE_ENV !== 'development'
				? [
						'http://localhost:8080',
						'http://127.0.0.1:8080',
						'http://localhost:5501',
						'http://127.0.0.1:5501',
				  ]
				: 'https://www.eliazoura.fr',
	},
});

io.on('connection', (socket) => {
	const userID = socket.id.substring(0, 5);
	console.log('USER connecté : ' + userID);

	socket.on('message', (data) => {
		console.log(data);
		io.emit('message', {user: userID, text: data});
	});

	// Vous pouvez ajouter plus de logique ici pour gérer les événements et les messages
});

// Démarrer le serveur HTTP
const PORT = process.env.PORT || 8080;
httpServer.listen(PORT, () => {
	console.log(`Server is running on port ${PORT}`);
});
